import React from 'react';
import Main from "./Main";
import keycloak from './keycloak'
import { ReactKeycloakProvider } from '@react-keycloak/web'

function App() {
  return (
    <ReactKeycloakProvider authClient={keycloak} >
      <Main/>
    </ReactKeycloakProvider>
  );
}

export default App;
