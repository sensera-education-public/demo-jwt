import React, {useCallback, useEffect, useState} from 'react';
import {useKeycloak} from "@react-keycloak/web";

export default function Main() {
    const {keycloak, initialized} = useKeycloak()


    const kcToken = keycloak?.token ?? '';
    const [users, setUsers] = useState<string[]>([])

    useEffect(() => {
            console.log("token", kcToken);
            if (kcToken) {
                fetch("http://localhost:8080/users", {
                    headers: {
                        "Authorization": "Bearer " + kcToken
                    }
                })
                    .then(res => res.json())
                    .then(users => setUsers(users))
                    .catch(reason => console.error("Failed to fetch ", reason))
            }
        },
        [keycloak.authenticated, kcToken])

    return (
        <>
            <h2>Hello</h2>
            {keycloak.authenticated ? <span>Authenticated</span> : <span>UnAuthenticated</span>}
            <div>
                <h4>Users</h4>
                <ul>
                    {users.map(user => <ul key={user}>{user}</ul>)}
                </ul>
            </div>
            <div>
                {keycloak.authenticated ?
                    <button
                        type="button"
                        onClick={() => keycloak.logout()}>
                        logout
                    </button>
                    :
                    <button
                        type="button"
                        onClick={() => keycloak?.login()}>
                        login
                    </button>
                }

            </div>
        </>
    )
}
