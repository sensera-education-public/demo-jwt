package com.example.api;

import com.example.api.jwt.KeyCloakToken;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureWebTestClient
class ApiApplicationTests {

    @Autowired
    WebTestClient webTestClient;
    String tokenString;

    @BeforeEach
    void setUp() {
        tokenString = KeyCloakToken.acquire("http://localhost:8000/",
                        "test",
                        "test-client",
                        "christian",
                        "password")
                .block()
                .getAccessToken();
    }

    @Test
    void test_users_access_success() {
        webTestClient
                .get().uri("/users")
                .headers(http -> http.setBearerAuth(tokenString))
                .exchange()
                .expectStatus().isOk();
    }

    @Test
    void test_users_access_failed_because_denied() {
        webTestClient
                .get().uri("/users")
                .exchange()
                .expectStatus().isUnauthorized();
    }
}
