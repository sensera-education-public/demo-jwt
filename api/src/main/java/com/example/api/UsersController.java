package com.example.api;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("users")
@CrossOrigin("*")
public class UsersController {

    //@RolesAllowed("user")
    @GetMapping
    public List<String> all() {
        return List.of("Arne","Agda","Beda");
    }

    //@RolesAllowed("user")
    @GetMapping("/name")
    public String name(Principal principal) {
        return principal.getName();
    }
}
